LOCATION OF CHANGES:

GRID_UNSTRUCTURED_EXPLICIT.F90:

l:1312-1330: this block is the if statment for the alternate upwinding methods,
             also a check for if upwinding fraction > 1 at l:1327 

l:1345:      output number of upwinding fratctions > 1, good sanity check if the
             hack is needed, i.e. something wrong with grid

l:1269-1273: variables needed for counting number of bad fractions and initialization


OPTION.F90:

l:227:       int in option class to determine which upwinding `method' to used in case 
             of explicit unstructured grid

l:588:       initialize option%upwind_weighting_method


DISCRETIZATION.F90:

l:154,243-245 AND 352,521-523: for reading in the upwinding option in the GRID card
